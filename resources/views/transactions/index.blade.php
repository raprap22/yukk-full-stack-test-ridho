<x-app-layout>
    <div class="max-w-2xl mx-auto py-6 sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form id="transactionForm" action="{{ route('transactions.store') }}" method="post"
                    enctype="multipart/form-data">
                    @csrf

                    <div>
                        <label for="type" class="block font-medium text-sm text-gray-700">Tipe:</label>
                        <select name="type" id="type"
                            class="block w-full mt-1 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="" disabled selected>Pilih Tipe</option>
                            <option value="top_up">Top Up</option>
                            <option value="transaction">Transaksi</option>
                        </select>
                    </div>
                    <div class="mt-4">
                        <label for="amount" class="block font-medium text-sm text-gray-700">Saldo:</label>
                        <input type="text" name="amount" id="amount" value="{{ old('amount') }}"
                            class="block w-full mt-1 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                            oninput="formatInput(this)">
                    </div>
                    <div class="mt-4">
                        <label for="description" class="block font-medium text-sm text-gray-700">Keterangan:</label>
                        <textarea name="description" id="description"
                            class="block w-full mt-1 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">{{ old('description') }}</textarea>
                    </div>
                    <div class="mt-4" id="proofImageDiv" style="display: none;">
                        <label for="proof_image" class="block font-medium text-sm text-gray-700">Bukti Top Up:</label>
                        <input type="file" name="proof_image" id="proof_image"
                            class="block w-full mt-1 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                    </div>

                    <div class="mt-4">
                        <button id="submitButton" type="submit"
                            class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        const typeSelect = document.getElementById('type');
        const proofImageDiv = document.getElementById('proofImageDiv');
        const submitButton = document.getElementById('submitButton');
        const transactionForm = document.getElementById('transactionForm');

        typeSelect.addEventListener('change', function() {
            if (this.value === 'top_up') {
                proofImageDiv.style.display = 'block';
            } else {
                proofImageDiv.style.display = 'none';
            }
        });
    });

    function formatRupiah(angka) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

        return 'Rp. ' + rupiah;
    }

    function unformatRupiah(rupiah) {
        return rupiah.replace(/[^0-9]/g, '');
    }

    function formatInput(input) {
        input.value = formatRupiah(unformatRupiah(input.value));
    }
</script>
