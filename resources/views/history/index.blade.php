<x-app-layout>
    <div class="max-w-2xl mx-auto py-6 sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-6 p-4">
            <h2 class="text-lg font-semibold mb-2">Saldo Saat Ini: Rp {{ number_format($currentBalance) }}</h2>
        </div>

        <div class="flex justify-between mb-4">
            <form action="{{ route('history') }}" method="GET" class="flex-1 mr-2">
                <div class="flex">
                    <input type="text" name="search" placeholder="Cari transaksi"
                        class="border-gray-300 rounded-l-md p-2 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 flex-1">
                    <button type="submit"
                        class="bg-indigo-500 text-white font-semibold px-4 rounded-r-md">Cari</button>
                </div>
            </form>

            <div class="flex items-center">
                <span class="text-gray-600">Urutkan:</span>
                <a href="{{ route('history', ['sort' => 'asc']) }}"
                    class="bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 ml-2">Ascending</a>
                <a href="{{ route('history', ['sort' => 'desc']) }}"
                    class="bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 ml-2">Descending</a>
            </div>
        </div>


        @if (count($transactions) > 0)
            @foreach ($transactions as $transaction)
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-4 p-4">
                    <h3 class="text-lg font-semibold mb-2">{{ $transaction->transaction_code }} <span
                            class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text- mr-2 mb-2">{{ $transaction->type === 'top_up' ? 'Top Up' : 'Transaksi' }}</span>
                    </h3>
                    <p class="text-gray-600">{{ $transaction->description }}</p>
                    <p class="text-gray-500">Jumlah: Rp {{ number_format($transaction->amount) }}</p>
                    <p class="text-gray-400">Tanggal: {{ $transaction->created_at }}</p>
                    @if ($transaction->type === 'top_up' && $transaction->proof_image)
                        <img src="{{ asset('/storage/proofs/' . $transaction->proof_image) }}" alt="Proof Image"
                            class="mt-4">
                    @endif
                </div>
            @endforeach

            <div class="flex justify-between mt-4">
                <div>
                    @if ($transactions->onFirstPage())
                        <span class="text-gray-400">&laquo; Sebelumnya</span>
                    @else
                        <a href="{{ $transactions->previousPageUrl() }}"
                            class="text-indigo-500 hover:text-indigo-700 transition duration-300">&laquo; Sebelumnya</a>
                    @endif
                </div>
                <div>
                    <span class="text-gray-400">Halaman {{ $transactions->currentPage() }} dari
                        {{ $transactions->lastPage() }}</span>
                </div>
                <div>
                    @if ($transactions->hasMorePages())
                        <a href="{{ $transactions->nextPageUrl() }}"
                            class="text-indigo-500 hover:text-indigo-700 transition duration-300">Selanjutnya
                            &raquo;</a>
                    @else
                        <span class="text-gray-400">Selanjutnya &raquo;</span>
                    @endif
                </div>
            </div>
        @else
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-4 p-4">
                <p class="text-gray-600 text-3xl font-bold text-center">Tidak ada transaksi</p>
            </div>
        @endif

    </div>
</x-app-layout>
