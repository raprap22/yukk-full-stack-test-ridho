<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{

    public function index(Request $request)
    {
        $query = Transaction::where('id_user', Auth::id())
            ->when($request->has('search'), function ($query) use ($request) {
                $query->where('transaction_code', 'like', '%' . $request->search . '%')
                    ->orWhere('description', 'like', '%' . $request->search . '%');
            });

        if ($request->has('sort')) {
            if ($request->sort === 'asc') {
                $query->orderBy('created_at', 'asc');
            } elseif ($request->sort === 'desc') {
                $query->orderBy('created_at', 'desc');
            }
        } else {
            $query->orderBy('created_at', 'desc');
        }

        $transactions = $query->paginate(3);

        $currentBalance = Transaction::where('id_user', Auth::id())->sum('amount');

        return view('history.index', compact('transactions', 'currentBalance'));
    }
}
