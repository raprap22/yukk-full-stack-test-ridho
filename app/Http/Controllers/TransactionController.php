<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransactionController extends Controller
{
    public function create()
    {
        return view('transactions.index');
    }

    public function store(Request $request)
    {
        $unformattedAmount = str_replace(['Rp', '.', ','], '', $request->amount);

        $request->merge(['amount' => intval($unformattedAmount)]);

        $validator = Validator::make($request->all(), [
            'type' => 'required|in:top_up,transaction',
            'amount' => ['required', 'numeric', Rule::in([intval($unformattedAmount)])],
            'description' => 'required|string|max:300',
            'proof_image' => 'required_if:type,top_up|image|mimes:jpg,png|max:2048',
        ]);

        if ($validator->fails()) {
            notify()->error('Gagal, mohon periksa kembali input Anda.');

            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($validator) {
            $transactionCode = 'TRX-' . uniqid();

            $transaction = new Transaction();
            $transaction->type = $request->type;
            $transaction->amount = intval($unformattedAmount);
            $transaction->description = $request->description;
            $transaction->transaction_code = $transactionCode;
            $transaction->id_user = Auth::id();

            if ($request->type == 'top_up' && $request->hasFile('proof_image')) {
                $proofImage = $request->file('proof_image');
                $proofImageName = 'proof_' . time() . '.' . $proofImage->getClientOriginalExtension();
                $proofImage->storeAs('/public/proofs', $proofImageName);
                $transaction->proof_image = $proofImageName;
            }


            $transaction->save();

            notify()->success('Transaksi berhasil disimpan.');
            return redirect()->route('history');
        } else {
            notify()->error('Validasi gagal, mohon periksa kembali input Anda.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }
}
